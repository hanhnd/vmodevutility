package com.vmodev.common.thread;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

public abstract class ConcurrentAsyncTask<Params, Progress, Result> extends
		AsyncTask<Params, Progress, Result> {

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void executeConcurrently(Params... params) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		} else {
			this.execute(params);
		}
	}

}
