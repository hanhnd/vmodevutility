package com.vmodev.common.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	public static Date convert(String txtTime, String pattern)
			throws ParseException {
		return convert(txtTime, pattern, Locale.getDefault());
	}

	public static Date convert(String txtTime, String pattern, Locale locale)
			throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern,
				locale);
		return simpleDateFormat.parse(txtTime);
	}

	public static String convert(Date date, String pattern, Locale locale) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern,
				locale);
		return simpleDateFormat.format(date);
	}

	public static String convert(Date date, String pattern) {
		return convert(date, pattern, Locale.getDefault());
	}
}
