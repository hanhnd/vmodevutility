package com.vmodev.common.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.graphics.Bitmap;

public class FileUtil {
	/**
	 * @param uri
	 * @return return file's name from a uri without extension
	 */
	public static String getFileName(String uri) {
		if (uri == null || uri.length() == 0) {
			return null;
		}
		int start = uri.lastIndexOf("/") + 1;
		if (start < 0) {
			start = 0;
		}
		int end = uri.lastIndexOf(".");
		if (end < 0) {
			end = uri.length();
		}
		return uri.substring(start, end);
	}

	/**
	 * @param uri
	 * @return return file's full name from a uri with extension
	 */
	public static String getFullFileName(String uri) {
		if (uri == null || uri.length() == 0) {
			return null;
		}
		int start = uri.lastIndexOf("/") + 1;
		if (start < 0) {
			start = 0;
		}
		return uri.substring(start, uri.length());
	}

	/**
	 * @param uri
	 * @return file's extension
	 */
	public static String getFileExtension(String uri) {
		if (uri == null || uri.length() == 0) {
			return null;
		}
		int start = uri.lastIndexOf(".");
		if (start < 0) {
			return "";
		}
		return uri.substring(start, uri.length() - 1);
	}

	/**
	 * Read text from file in assets folder
	 * 
	 * @throws IOException
	 */
	public static String readTextFileAssets(Context context, String uri,
			String charsetName) throws IOException {
		StringBuilder out = new StringBuilder();
		InputStream stream = context.getAssets().open(uri);
		BufferedReader in = new BufferedReader(new InputStreamReader(stream,
				charsetName));
		String line = null;
		while ((line = in.readLine()) != null) {
			out.append(line);
		}
		in.close();
		return out.toString();
	}

	public static boolean saveBitmapToFile(Bitmap b, String strFileName) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(strFileName);
			if (null != fos) {
				b.compress(Bitmap.CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();
				return true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
