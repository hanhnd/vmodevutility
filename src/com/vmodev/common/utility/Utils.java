package com.vmodev.common.utility;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Utils {
	public static void startActivity(Context context, Class<?> activityClass) {
		Intent intent = new Intent(context, activityClass);
		context.startActivity(intent);
	}

	public static void switchActivity(Activity activity, Class<?> activityClass) {
		Intent intent = new Intent(activity, activityClass);
		activity.startActivity(intent);
		activity.finish();
	}

	public static void startService(Context context, Class<?> service) {
		Intent intent = new Intent(context, service);
		context.startService(intent);
	}
}
