package com.vmodev.common.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class NotificationUtil {
	/**
	 * Show string message
	 * 
	 * @param context
	 * @param ex
	 */
	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	/**
	 * Show exception message
	 * 
	 * @param context
	 * @param ex
	 */
	public static void showToast(Context context, Exception ex) {
		Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
	}

	/**
	 * Show string by id
	 * 
	 * @param context
	 * @param ex
	 */
	public static void showToast(Context context, int strId) {
		Toast.makeText(context, strId, Toast.LENGTH_LONG).show();
	}

	/**
	 * Create and show a simple confirm dialog
	 * 
	 * @param context
	 * @param titleId
	 * @param messageId
	 * @param textPositiveId
	 * @param textNegativeId
	 * @param iconId
	 * @param onPositiveButton
	 * @param onNegativeButton
	 */
	public static void showConfirmDialog(Context context, int titleId,
			int messageId, int textPositiveId, int textNegativeId, int iconId,
			DialogInterface.OnClickListener onPositiveButton,
			DialogInterface.OnClickListener onNegativeButton) {
		String strBtnOk = context.getString(textPositiveId);
		String strBtnCancel = context.getString(textNegativeId);
		new AlertDialog.Builder(context).setIcon(iconId).setTitle(titleId)
				.setMessage(messageId)
				.setPositiveButton(strBtnOk, onPositiveButton)
				.setNegativeButton(strBtnCancel, onNegativeButton).create()
				.show();
	}
}