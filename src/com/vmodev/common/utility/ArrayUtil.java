package com.vmodev.common.utility;


import java.util.ArrayList;
import java.util.List;

import android.util.SparseArray;

public class ArrayUtil {
	/***********
	 * Sparse Array
	 */
	public static List<?> convert(SparseArray<?> sparseArray) {
		if (sparseArray != null) {
			List<Object> list = new ArrayList<Object>();
			int length = sparseArray.size();
			for (int index = 0; index < length; index++) {
				list.add(sparseArray.valueAt(index));
			}
			return list;
		}
		return null;
	}
}
