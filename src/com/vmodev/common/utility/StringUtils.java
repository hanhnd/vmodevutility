package com.vmodev.common.utility;
/**
Leo
 */


public class StringUtils {

	/**
	 * 
	 * @param obj
	 * @return true if obj is null
	 */
	public static boolean isNull(Object obj) {
		return obj == null;
	}

	/**
	 * 
	 * @param obj
	 * @return true if obj is null and is empty
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.equals("");
	}

	/**
	 * 
	 * @param obj
	 * @return true if obj is null and is empty
	 */
	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}

	// son.le
	/**
	 *  upper the first char
	 * @param strToParse
	 * @return
	 */
	public static String toUpperCaseFirstChar(String strToParse) {
		if (strToParse != null && !strToParse.isEmpty()) {
			char[] stringArray = strToParse.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			strToParse = new String(stringArray);
		} else {
			strToParse = "";
		}
		return strToParse;
	}
}
