package com.vmodev.common.utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

public class ScreenUtil {
	public static int[] getScreenSize(Activity activity) {
		int measuredWidth = 0;
		int measuredHeight = 0;
		Point size = new Point();
		WindowManager window = activity.getWindowManager();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			getScreenSizeHoneyComb(window, size);
			measuredWidth = size.x;
			measuredHeight = size.y;
		} else {
			Display d = window.getDefaultDisplay();
			measuredWidth = d.getWidth();
			measuredHeight = d.getHeight();
		}
		return new int[] { measuredWidth, measuredHeight };
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private static void getScreenSizeHoneyComb(WindowManager window, Point size) {
		window.getDefaultDisplay().getSize(size);
	}
}
