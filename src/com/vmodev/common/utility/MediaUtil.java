package com.vmodev.common.utility;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class MediaUtil {
	public static String getAudioPatch(Context context, Uri uri) {
		String filePath = null;
		if (uri != null && "content".equals(uri.getScheme())) {
			Cursor cursor = context
					.getContentResolver()
					.query(uri,
							new String[] { android.provider.MediaStore.Audio.AudioColumns.DATA },
							null, null, null);
			if (!cursor.moveToFirst()) {
				filePath = uri.getPath();
			} else {
				filePath = cursor.getString(0);
			}
			cursor.close();
		} else {
			filePath = uri.getPath();
		}
		return filePath;
	}
}
