package com.vmodev.common.utility;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings.Secure;

public class OsUtil {
	public static void openUrl(Context context, String url) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		context.startActivity(i);
	}

	public static void shareContent(Context context, String type,
			Intent extras, String titleChooser) {
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setType(type);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		intent.putExtras(extras);
		context.startActivity(Intent.createChooser(intent, titleChooser));
	}

	public static void openAppOnPlayMarket(Context context) {
		// getPackageName() from Context or Activity object
		final String appPackageName = context.getPackageName();
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse("http://play.google.com/store/apps/details?id="
							+ appPackageName)));
		}
	}

	public static String getDeviceId(Context context) {
		String deviceId = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
		return deviceId;
	}
}
