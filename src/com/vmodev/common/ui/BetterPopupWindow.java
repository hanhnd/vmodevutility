package com.vmodev.common.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

public abstract class BetterPopupWindow extends PopupWindow {

	private Context context;

	public BetterPopupWindow(Context context) {
		super(context);
		this.context = context;
	}

	protected void setContentView(int resId) {
		View view = LayoutInflater.from(context).inflate(resId, null);
		setContentView(view);
	}
}
