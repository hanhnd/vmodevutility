/**
Leo
 */
package com.vmodev.common.log;

import android.util.Log;

public class Vlog {
	public static Boolean showLog = true;

	private static String getTag(Object clazz) {
		String tag = null;
		if (clazz != null) {
			tag = clazz.getClass().getSimpleName();
		} else {
			tag = "Unknow";
		}
		return tag;
	}

	public static void i(Object clazz, String msg) {
		if (showLog) {
			Log.i(getTag(clazz), msg != null ? msg : "Null");
		}
	}

	public static void d(Object clazz, String msg) {
		if (showLog) {
			Log.d(getTag(clazz), msg != null ? msg : "Null");
		}
	}

	public static void w(Object clazz, String msg) {
		if (showLog) {
			Log.w(getTag(clazz), msg != null ? msg : "Null");
		}
	}

	public static void e(Object clazz, String msg) {
		if (showLog) {
			Log.e(getTag(clazz), msg != null ? msg : "Null");
		}
	}

	public static void v(Object clazz, String msg) {
		if (showLog) {
			Log.v(getTag(clazz), msg != null ? msg : "Null");
		}
	}

	public static boolean isShowLog() {
		return showLog;
	}

	public static void setShowLog(boolean showLog) {
		Vlog.showLog = showLog;
	}

	public static void i(Object clazz, String msg, boolean newShowLog) {
		if (showLog) {
			Log.i(getTag(clazz), msg != null ? msg : "Null");
		}
		showLog = newShowLog;
	}

	public static void d(Object clazz, String msg, boolean newShowLog) {
		if (showLog) {
			Log.d(getTag(clazz), msg != null ? msg : "Null");
		}
		showLog = newShowLog;
	}

	public static void w(Object clazz, String msg, boolean newShowLog) {
		if (showLog) {
			Log.w(getTag(clazz), msg != null ? msg : "Null");
		}
		showLog = newShowLog;
	}

	public static void e(Object clazz, String msg, boolean newShowLog) {
		if (showLog) {
			Log.e(getTag(clazz), msg != null ? msg : "Null");
		}
		showLog = newShowLog;
	}

	public static void v(Object clazz, String msg, boolean newShowLog) {
		if (showLog) {
			Log.v(getTag(clazz), msg != null ? msg : "Null");
		}
		showLog = newShowLog;
	}
}
